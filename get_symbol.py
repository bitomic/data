from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json
from db_actions import db_query

url = 'https://api.binance.com/api/v3/exchangeInfo'


def exchange_info(url):
    try:
        exchange = 'Binance'
        exchange_lower = exchange.lower()
        if exchange_lower in url:
            sql = "SELECT id FROM exchanges WHERE exchange_name = %s"
            entry = ('Binance',)
            type = 'select'
            exchange_id = db_query(sql, entry, type)

        return exchange_id
    except (RunTimeError, TypeError, NameError) as error:
        print(error)


def get_symbols(url):

    coins = []
    get_symbols_response = {}
    session = Session()
    try:
        response = session.get(url)
        data = json.loads(response.text)
        coin_pairs = []
        for key in data:
            if key == 'symbols':
                for i in data[key]:
                    get_symbols = i.items()
                    temp_base_asset = ''
                    temp_pair = ''
                    temp_quote_asset = ''
                    for j in get_symbols:
                        if j[0] == 'baseAsset':
                            temp_base_asset = j[1]
                            coins.append(j[1])
                        if j[0] == 'symbol':
                            temp_pair = j[1]
                        if j[0] == 'quoteAsset':
                            temp_quote_asset = j[1]
                            coins.append(j[1])
                    coin_pairs.append(
                        {'pairs': temp_pair, 'base': temp_base_asset, 'quote': temp_quote_asset})

        get_symbols_response = {
            'coin_symbol': coins, 'coin_pair_list': coin_pairs}

        return get_symbols_response

    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)


def populate_coin_symbol_in_coins_tb(url):
    try:
        data = get_symbols(url)
        type = 'insert'
        sql = "INSERT INTO coins (name, website_1, website_2, document_1, document_2, binance, about) VALUES (%s, %s, %s, %s, %s, %s, %s);"
        coin_sans_duplicate = list(dict.fromkeys(data['coin_symbol']))

        with open('coinsMaster.json') as ref:
            info = json.load(ref)
            for x in coin_sans_duplicate:
                for i in info:
                    if i['symbol'] == x:
                        coin_name = i['name']
                        # print("If {} is equal to {}".format(i['symbol'], x))
                        if 'website_1' not in i.keys():
                            website1 = None
                        else:
                            website1 = i['website_1']
                        if 'website_2' not in i.keys():
                            website2 = None
                        else:
                            website2 = i['website_2']
                        if 'document_1' not in i.keys():
                            document1 = None
                        else:
                            document1 = i['document_1']
                        if 'document_2' not in i.keys():
                            document2 = None
                        else:
                            document2 = i['document_2']
                        if 'about' not in i.keys():
                            about = None
                        else:
                            about = i['about']

                entry = (coin_name, website1,
                         website2, document1, document2, x, about,)

                db_query(sql, entry, type)
        print("Coins table updated....")
    except (RuntimeError, TypeError, NameError) as error:
        print(error)


populate_coin_symbol_in_coins_tb(url)
# get_symbols(url)
# exchange_info(url)

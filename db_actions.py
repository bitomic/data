import psycopg2
from psycopg2 import Error
from config import config


def db_query(sql, entry, type):
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()

        if type == 'insert':
            cur.execute(sql, entry)
        if type == 'select':
            cur.execute(sql, entry)
            res = cur.fetchall()
            return res

        conn.commit()
        cur.close()

    except(Exception, psycopg2.DatabaseError) as error:
        conn.rollback()
        print('Error-', error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    db_query()

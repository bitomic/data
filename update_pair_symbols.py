from get_symbol import exchange_info, get_symbols
from db_actions import db_query

url = 'https://api.binance.com/api/v3/exchangeInfo'


# def get_exchange_pairs(url):
#     try:
#         exchange_id = exchange_info(url)
#         return exchange_id[0][0]
#     except (RuntimeError, TypeError, NameError) as error:
#         print(error)


def populate_exchange_pairs(url):
    try:
        get_exchange_id = exchange_info(url)
        exchange_id = get_exchange_id[0][0]
        type1 = 'insert'
        type = 'select'
        sql1 = "INSERT INTO exchange_pairs (exchange_id, quote_coin_id, base_coin_id, pair_symbol) VALUES (%s, %s, %s, %s);"
        coin_info = get_symbols(url)

        sql = "SELECT id FROM coins WHERE symbol = %s"
        # base_id = db_query(
        #     sql, (coin_info['coin_pair_list'][0]['base'],), type)
        # quote_id = db_query(
        #     sql, (coin_info['coin_pair_list'][0]['quote'],), type)
        # print(base_id[0][0], quote_id[0][0])
        c = 0
        for i in coin_info['coin_pair_list']:
            c = c+1
            base_id = db_query(sql, (i['base'],), type)
            quote_id = db_query(sql, (i['quote'],), type)
            pair = i['pairs']
            entry = (exchange_id, quote_id[0][0], base_id[0][0], pair,)
            print(c, entry)

            # populate = db_query(sql1, entry, type1)

        # print("exchanges table populated...")

        # sql= "INSERT INTO exchange_pairs (exchange_id, pair_symbol) VALUES (%s, %s);"
    except(RuntimeError, TypeError, NameError) as error:
        print(error)


populate_exchange_pairs(url)
# binance_exchange_pairs(url)
